﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest01
{
    internal class No7
    {
        public No7()
        {
            Console.WriteLine();
            Console.WriteLine("---No 7---");
            Console.WriteLine();
            // ICE CREAM BELI 6 GRATIS 1 HARGA 100
            Console.Write("Input the nominal of money = ");
            int money = int.Parse(Console.ReadLine());

            int iceCreamCost = 100;

            int iceCreamGet = money / iceCreamCost;

            for (int i = 6; i <= iceCreamGet; i+=6)
            {
                iceCreamGet += 1;
            }

            Console.WriteLine();
            Console.WriteLine($"With money {money} can get {iceCreamGet}");
            Console.WriteLine();

            Console.WriteLine();
            Console.WriteLine("Press 1 to continue this number or any key to exit ");
            string choice = Console.ReadLine();
            if (choice == "1")
            {
                No7 no7 = new();
            }
        }
    }
}
