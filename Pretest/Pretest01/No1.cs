﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



//Soal 01
//Cari jumlah terbesar dan terkecil dengan mengambil masing2 1 item di jenis belanja(ambil 2
//item yg berbeda).
//Input:
//Topi 10 7 19
//Kemeja 20 25 18
//Output:
//25 44

namespace Pretest01
{
    internal class No1
    {
        public No1()
        {
            Console.WriteLine();
            Console.WriteLine("===NOMOR 1===");
            Console.Write("Masukkan nilai jenis belanja Topi : ");
            int n_topi = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai jenis belanja Kemeja : ");
            int n_kemeja = int.Parse(Console.ReadLine());

            //int[] topi = { 10, 7, 19 };
            //int[] kemeja = { 20, 25, 18 };

            int[] topi = new int[n_topi];
            int[] kemeja = new int[n_kemeja];
            int min_t, min_k, max_t, max_k;

            Console.WriteLine("Banyak Topi = ");
            for (int i = 0; i < n_topi; i++)
            {
                topi[i] = int.Parse(Console.ReadLine());
            }

            Console.WriteLine("Banyak Kemeja = ");
            for (int i = 0; i < n_kemeja; i++)
            {
                kemeja[i] = int.Parse(Console.ReadLine());
            }

            min_t = topi[0];
            max_t = topi[0];
            for (int i = 1; i < topi.Length; i++)
            {
                if (topi[i] < topi[i-1])
                {
                    min_t = topi[i];
                }
                if (topi[i] > topi[i - 1])
                {
                    max_t = topi[i];
                }
            }

            min_k = kemeja[0];
            max_k = kemeja[0];
            for (int i = 1; i < kemeja.Length; i++)
            {
                if (kemeja[i] < kemeja[i - 1])
                {
                    min_k = kemeja[i];
                }
                if (kemeja[i] > kemeja[i - 1])
                {
                    max_k = kemeja[i];
                }
            }

            Console.WriteLine($"Terbesar = {max_t + max_k}");
            Console.WriteLine($"Terkecil = {min_t + min_k}");
            Console.WriteLine();

            Console.WriteLine();
            Console.WriteLine("Press 1 to continue this number or any key to exit ");
            string choice = Console.ReadLine();
            if (choice == "1")
            {
                No1 no1 = new();
            }
        }
    }
}
