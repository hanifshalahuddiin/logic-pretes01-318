﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Soal 03
//Urutkan perpaduan huruf dan angka. Huruf terlebih dahulu disusul dengan angka
//Input:
//s9nn10
//Output:
//nns019

namespace Pretest01
{
    internal class No3
    {
        public No3()
        {
            Console.WriteLine(  );
            Console.WriteLine("===NOMOR 3===");
            Console.Write("Masukkan string : ");
            string input = Console.ReadLine();

            string strings = "abcdefghijklmnopqrstuvwxyz0123456789 ";
            char[] chars = strings.ToCharArray();
            int[] index_chars = new int[chars.Length];

            //string input = "s9nn10";
            char[] char_input = input.ToCharArray();
            int[] index_input = new int[char_input.Length];

            for (int i = 0; i < chars.Length; i++)
            {
                index_chars[i] = i;
            }

            for (int i = 0; i < char_input.Length; i++)
            {
                for (int j = 0; j < chars.Length; j++)
                {
                    if (char_input[i] == chars[j])
                    {
                        index_input[i] = j;
                    }
                }
            }

            int temp;
            for (int i = 0; i < char_input.Length - 1; i++)
            {
                for (int j = i+1; j < char_input.Length; j++)
                {
                    if (index_input[i] > index_input[j])
                    {
                        temp = index_input[i];
                        index_input[i] = index_input[j];
                        index_input[j] = temp;
                    }
                }
            }

            for (int i = 0; i < char_input.Length; i++)
            {
                for (int j = 0; j < chars.Length; j++)
                {
                    if (index_input[i] == index_chars[j])
                    {
                        char_input[i] = chars[j];
                    }
                }
            }

            // PAK ATUR
            //for (int i = 1; i < char_input.Length; i++)
            //{
            //    for (int j = i; j > 0; j--)
            //    {
            //        if (strings.IndexOf(char_input[j]) < strings.IndexOf(char_input[i]))
            //        {
            //            var tempo = char_input[j];
            //            char_input[j] = char_input[j - 1];
            //            char_input[j - 1] = tempo;
            //        }
            //    }
            //}

            Console.Write($"Hasilnya ");
            for (int i = 0; i < char_input.Length; i++)
            {
                Console.Write(char_input[i]);
            }
            Console.WriteLine();

            foreach (var item in char_input)
            {
                Console.Write(item);
            }

            Console.WriteLine();
            Console.WriteLine("Press 1 to continue this number or any key to exit ");
            string choice = Console.ReadLine();
            if (choice == "1")
            {
                No3 no3 = new();
            }
        }
    }
}
