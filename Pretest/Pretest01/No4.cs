﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest01
{
    internal class No4
    {
        public No4()
        {
            Console.WriteLine();
            Console.WriteLine("===NOMOR 4===");
            Console.Write("Masukkan tanggal pinjam (yyyy-MM-dd) : ");
            string pinjam = Console.ReadLine();
            Console.Write("Masukkan tanggal kembali (yyyy-MM-dd) : ");
            string kembali = Console.ReadLine();

            string[] pinjam2 = pinjam.Split('-');
            string[] kembali2 = kembali.Split('-');
            DateTime datePinjam = new DateTime(int.Parse(pinjam2[0]), int.Parse(pinjam2[1]), int.Parse(pinjam2[2]));
            DateTime dateKembali = new DateTime(int.Parse(kembali2[0]), int.Parse(kembali2[1]), int.Parse(kembali2[2]));

            Console.WriteLine(datePinjam);
            Console.WriteLine(dateKembali);

            TimeSpan diff = dateKembali - datePinjam;
            int hari = diff.Days;
            int kal = 5, sda = 7, mat = 4, denda;
            if (hari <= 5)
            {
                kal = 0;
            }
            else
            {
                kal = hari - 5;
            }
            if (hari <= 7)
            {
                sda = 0;
            }
            else
            {
                sda = hari - 7;
            }
            if (hari <= 4)
            {
                mat = 0;
            }
            else
            {
                mat = hari - 4;
            }
            denda = (kal * 1000) + (sda * 1500) + (mat * 750);
            Console.WriteLine();
            Console.WriteLine($"Total Hari = {hari}");
            Console.WriteLine($"Denda Kalkulus = {kal*=1000}");
            Console.WriteLine($"Denda Struktur Data = {sda *= 1500}");
            Console.WriteLine($"Denda Matematika = {mat *= 750}");
            Console.WriteLine($"Total Denda = {denda}");
            Console.WriteLine();

            Console.WriteLine();
            Console.WriteLine("Press 1 to continue this number or any key to exit ");
            string choice = Console.ReadLine();
            if (choice == "1")
            {
                No4 no4 = new();
            }
        }
    }
}
