﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest01
{
    internal class No5
    {
        public No5()
        {
            Console.WriteLine();
            Console.WriteLine("===NOMOR 5===");
            Console.Write("Masukkan ukuran persegi : ");
            int sisi = int.Parse(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine($"Ukuran persegi = {sisi} x {sisi}");
            Console.WriteLine();

            for (int i = 0; i < sisi; i++)
            {
                for (int j = 0; j < sisi; j++)
                {
                    if (i == 0 || i == sisi-1 || j == 0 || j == sisi-1)
                    {
                        Console.Write($" *");
                    }
                    else if (i == j || i+j == sisi-1)
                    {
                        if (sisi%2 == 0)
                        {
                            if (i==sisi/2-1 || i==sisi/2)
                            {
                                Console.Write($" {sisi}");
                            }
                            else
                            {
                                Console.Write($" *");
                            }
                        }
                        else
                        {
                            if (i == j && i + j == sisi - 1)
                            {
                                Console.Write($" {sisi}");
                            }
                            else
                            {
                                Console.Write($" *");
                            }
                        }
                    }
                    else
                    {
                        Console.Write($"  ");
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();

            Console.WriteLine();
            Console.WriteLine("Press 1 to continue this number or any key to exit ");
            string choice = Console.ReadLine();
            if (choice == "1")
            {
                No5 no5 = new();
            }
        }
    }
}
