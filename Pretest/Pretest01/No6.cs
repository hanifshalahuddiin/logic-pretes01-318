﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest01
{
    internal class No6
    {
        public No6()
        {
        lanjut:
            Console.WriteLine();
            Console.WriteLine("===NOMOR 6===");
            Console.WriteLine(DateTime.Now);
            Console.Write("Masukkan tanggal dan waktu masuk : ");
            string[] Masuk = Console.ReadLine().Split('T');
            string[] tglMasuk = Masuk[0].Split('-');
            string[] wktMasuk = Masuk[1].Split(':');
            DateTime dateTimeMasuk = new DateTime(int.Parse(tglMasuk[0]), int.Parse(tglMasuk[1]), int.Parse(tglMasuk[2]), int.Parse(wktMasuk[0]), int.Parse(wktMasuk[1]), int.Parse(wktMasuk[2]));
            Console.Write("Masukkan tanggal dan waktu keluar : ");
            string[] Keluar = Console.ReadLine().Split('T');
            string[] tglKeluar = Keluar[0].Split('-');
            string[] wktKeluar = Keluar[1].Split(':');
            DateTime dateTimeKeluar = new DateTime(int.Parse(tglKeluar[0]), int.Parse(tglKeluar[1]), int.Parse(tglKeluar[2]), int.Parse(wktKeluar[0]), int.Parse(wktKeluar[1]), int.Parse(wktKeluar[2]));

            TimeSpan selisih = dateTimeKeluar - dateTimeMasuk;

            double totJamDob = selisih.TotalHours;
            int totJamInt = (int)totJamDob;
            double jamDob = totJamDob - totJamInt;
            int jam = totJamInt;
            if (jamDob > 0)
            {
                jam = totJamInt + 1;
            }

            int harga = 0;
            int banyakHari, sisaJam;
            //1 5k, 2-7 3k, 8-12 2k, 13-23 1k, 24 20k, 25 dst

            banyakHari = jam / 24;
            sisaJam = jam % 24;

            if (banyakHari > 0)
            {
                harga += banyakHari * 20000;
                Console.WriteLine(harga);
            }

            if (sisaJam > 0)
            {
                harga += 5000;
                sisaJam -= 1;
            }
            if (sisaJam > (24 - 1 - (23 - 13 + 1)))
            {
                harga += 1000 * (sisaJam - (7 - 2 + 1) - (12 - 8 + 1));
                sisaJam -= 0 - (7 - 2 + 1) - (12 - 8 + 1);
            }
            if (sisaJam > (24 - 1 - (23 - 13 + 1) - (12 - 8 + 1)))
            {
                harga += 2000 * (sisaJam - (12 - 8 + 1));
                sisaJam -= 0 - (12 - 8 + 1);
            }
            if (sisaJam > (24 - 1 - (23 - 13 + 1) - (12 - 8 + 1) - (7 - 2 + 1)))
            {
                harga += 3000 * sisaJam;
            }
            Console.WriteLine();
            Console.WriteLine($"Parkir yang harus dibayar = Rp {harga}");
            Console.WriteLine();

            Console.WriteLine();
            Console.WriteLine("Press 1 to continue this number or any key to exit ");
            string choice = Console.ReadLine();
            if (choice == "1")
            {
                No6 no6 = new();
            }
        }
    }
}
