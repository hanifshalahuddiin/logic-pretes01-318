﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest01
{
    internal class No8
    {
        public No8()
        {
            Console.WriteLine();
            Console.WriteLine("---No 8---");
            Console.WriteLine();
            //SELISIH VOKAL DAN KONSONAN
            Console.Write("Input string with vocal and consonan = ");
            string sentence = Console.ReadLine();

            string alphabet = "0abcdefghijklmnopqrstuvwxyz";
            char[] letter = alphabet.ToArray();
            int[] index = new int[27];
            int vocal = 0, consonan = 0;

            for (int i = 0; i <= 26; i++)
            {
                index[i] = i;
            }

            for (int i = 0; i <= sentence.Length; i++)
            {
                if (letter[i] == 'a' || letter[i] == 'e' || letter[i] == 'i' || letter[i] == 'o' || letter[i] == 'u')
                {
                    vocal += index[i];
                }
                else
                {
                    consonan += index[i];
                }
            }

            Console.WriteLine();
            if (vocal >= consonan)
            {
                Console.WriteLine($"Selisih vocal = {vocal} dan consonan = {consonan} adalah {vocal - consonan}");
            }
            else
            {
                Console.WriteLine($"Selisih consonan = {consonan} dan vocal = {vocal} adalah {consonan - vocal}");
            }
            Console.WriteLine();

            Console.WriteLine();
            Console.WriteLine("Press 1 to continue this number or any key to exit ");
            string choice = Console.ReadLine();
            if (choice == "1")
            {
                No8 no8 = new();
            }
        }
    }
}
