﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    internal class No3
    {
        public No3()
        {
//            03 Triangle numbers
//Input:
//            0 9 4
//            - 1 2 10
//1 8 3
//Output:
//            0 + 9 + 4 + 2 + 10 + 3 = 28
//0 - 1 + 2 + 1 + 8 + 3 = 13
//28 + 13 = 41
            Console.WriteLine();
            Console.WriteLine("===NO 3===");
            Console.WriteLine();

            Console.Write("Masukkanlah ukuran persegi = ");
            int ukuran = int.Parse(Console.ReadLine());
            //int ukuran = 3;
            //Console.WriteLine("Masukkanlah angka sebanyak 9x = ");

            int i, j, k;
            int[] triangle = new int[ukuran*ukuran];
            k = 0;
            for (i = 0; i < ukuran; i++)
            {
                for (j = 0; j < ukuran; j++)
                {
                    Console.Write($"Angka[{k + 1}] = ");
                    triangle[k] = int.Parse(Console.ReadLine());
                    k++;
                }
            }
            //int[] triangle = { 0, 9, 4, -1, 2, 10, 1, 8, 3 };

            k = 0;
            for (i = 0; i < ukuran; i++)
            {
                for(j = 0; j < ukuran; j++)
                {
                    Console.Write($" {triangle[k]}");
                    k++;
                }
                Console.WriteLine();
            }
            Console.WriteLine();

            int jumlaha = 0, jumlahb = 0;

            k = 0;
            for (i = 0; i < ukuran; i++)
            {
                for (j = 0; j < ukuran; j++)
                {
                    if (i <= j)
                    {
                        jumlaha += triangle[k];
                    }
                    if (i >= j)
                    {
                        jumlahb += triangle[k];
                    }
                    k++;
                }
            }
            Console.WriteLine();
            Console.WriteLine($"Jumlah atas = {jumlaha}");
            Console.WriteLine($"Jumlah bawah = {jumlahb}");
            Console.WriteLine($"Jumlah total = {jumlaha + jumlahb}");

            Console.WriteLine();
            Console.WriteLine("Press 1 to continue this number or any key to exit ");
            string choice = Console.ReadLine();
            if (choice == "1")
            {
                No3 execute = new();
            }
        }
    }
}
