﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    internal class No1
    {
        public No1()
        {
//            01 Rotasi group number dimana memasukkan jumlah angka dalam 1 group, jumlah putaran dan
//selanjutnya deret angka kelipatan jumlah 1 group
//Input 1:
//3
//2
//129762982
//Output 1:
//982129762
//Penjelasan
//129 762 982
//762 982 129
//982 129 762
//Input 2:
//4
//1
//1297629823429821
//Output 2:
//6298234298211297
//Penjelasan
//1297 6298 2342 9821
//6298 2342 9821 1297
//Input 3:
//4
//1
//129762982342982
//Output 3:
//FAIL
            Console.WriteLine();
            Console.WriteLine("===NO 1===");
            Console.WriteLine();

            Console.Write("Masukkan banyak angka dalam 1 grup = ");
            int n = int.Parse(Console.ReadLine());
            //int n = 3;

            Console.Write("Masukkan banyak putaran = ");
            int r = int.Parse(Console.ReadLine());
            //int r = 2;

            Console.Write("Masukkan angka = ");
            string angka = Console.ReadLine();
            //string angka = "129762982";

            if (angka.Length % n != 0)
            {
                Console.WriteLine("Panjang deret angka harus merupakan kelipatan dari banyak angka dalam 1 grup");
                goto selesai;
            }
            int[] intangka = new int[angka.Length];
            int[,] angka2d = new int[angka.Length/n,n];

            int i, j, k;
            for (i = 0; i < angka.Length; i++)
            {
                intangka[i] = Int32.Parse(angka.Substring(i, 1));
            }

            k = 0;
            for (i = 0; i < angka.Length/n; i++)
            {
                for (j = 0; j < n; j++)
                {
                    angka2d[i, j] = intangka[k];
                    k++;
                }
            }

            Console.WriteLine($"Hasil sebelum rotasi adalah = ");

            for (i = 0; i < angka.Length / n; i++)
            {
                for (j = 0; j < n; j++)
                {
                    Console.Write($"{angka2d[i, j]}");
                }
                Console.Write(" ");
            }

            int[,] angka2dimit = angka2d;
            int[] imitasi = new int[angka.Length/n];

            for (i = 0; i < r; i++)
            {
                for (j = 0; j < n; j++)
                {
                    imitasi[j] = angka2d[0, j];
                }
                for (j = 0; j < angka.Length/n - 1; j++)
                {
                    for (k = 0; k < n; k++)
                    {
                        angka2d[j, k] = angka2d[j + 1, k];
                    }
                }
                for (k = 0; k < n; k++)
                {
                    angka2d[angka.Length/n - 1, k] = imitasi[k];
                }
            }

            Console.WriteLine($"Hasil sesudah rotasi {r}x adalah = ");

            for (i = 0; i < angka.Length/n; i++)
            {
                for (j = 0; j < n; j ++)
                {
                    Console.Write($"{angka2d[i,j]}");
                }
                Console.Write(" ");
            }

        selesai:

            Console.WriteLine();
            Console.WriteLine("Press 1 to continue this number or any key to exit ");
            string choice = Console.ReadLine();
            if (choice == "1")
            {
                No1 execute = new();
            }
        }
    }
}
