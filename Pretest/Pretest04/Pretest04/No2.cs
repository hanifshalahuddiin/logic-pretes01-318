﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    internal class No2
    {
        public No2()
        {
//            02 Difference ganjil genap
//Input
//0321837
//Output
//4
//Penjelasan:
//            0 + 2 + 8 = 10
//3 + 1 + 3 + 7 = 14
//10 - 14 = 4(absolut)
            Console.WriteLine();
            Console.WriteLine("===NO 2===");
            Console.WriteLine();

            Console.Write("Masukkanlah deretan angka = ");
            string deret = Console.ReadLine();

            //string deret = "0321837";

            char[] angkachar = deret.ToCharArray();

            int i, j;
            int[] angka = new int[deret.Length];

            for (i = 0; i < deret.Length; i++)
            {
                angka[i] = Int32.Parse(deret.Substring(i,1));
            }

            int genap = 0;
            int gasal = 0;
            for (i = 0; i < deret.Length; i++)
            {
                if (angka[i] % 2 == 0)
                {
                    genap += angka[i];
                }
                else
                {
                    gasal += angka[i];
                }
            }

            int max, min;

            if (genap > gasal)
            {
                max = genap; min = gasal;
            }
            else
            {
                max = gasal; min = genap;
            }

            Console.WriteLine($"Hasil penjumlahan genap adalah = {genap}");
            Console.WriteLine($"Hasil penjumlahan gasal adalah = {gasal}");
            Console.WriteLine($"Hasil selisihnya adalah = {max - min}");

            Console.WriteLine();
            Console.WriteLine("Press 1 to continue this number or any key to exit ");
            string choice = Console.ReadLine();
            if (choice == "1")
            {
                No2 execute = new();
            }
        }
    }
}
