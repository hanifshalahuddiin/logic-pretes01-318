﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    internal class No4
    {
        public No4()
        {
//            04 Summary alphabet and sort
//Input:
//            helloworld
//            Output:
//dehlorw

            Console.WriteLine();
            Console.WriteLine("===NO 4===");
            Console.WriteLine();
            Console.Write("Masukkan kata = ");
            //string kata = Console.ReadLine();
            string kata = "helloworld";
            int[] indkata = new int[kata.Length];

            string alphabet = "0123456789abcdefghijklmnopqrstuvwxyz";
            int[] indalpha = new int[alphabet.Length];

            int i, j, k;

            for (i = 0; i < alphabet.Length; i ++)
            {
                indalpha[i] = i;
            }

            char[] kata2 = new char[kata.Length];
            for (i = 0; i < kata.Length; i++)
            {
                for (j = 0; j < alphabet.Length; j ++)
                {
                    if (kata[i] == alphabet[j])
                    {
                        indkata[i] = j;
                    }
                }
            }

            for (i = 0; i < kata.Length - 1; i++)
            {
                for (j = i + 1; j < kata.Length; j ++)
                {
                    if (indkata[i] > indkata[j])
                    {
                        int hm = indkata[i];
                        indkata[i] = indkata[j];
                        indkata[j] = hm;
                    }
                }
            }

            int skip = 0;
            for (i = 0; i < kata.Length; i++)
            {
                for (j = 0; j < alphabet.Length; j ++)
                {
                    if (indkata[i] == j)
                    {
                        skip = 0;
                        for (k = 0; k < kata2.Length; k ++)
                        {
                            if (indkata[i] == indkata[k])
                            {
                                skip++;
                                //kata2[i] = alphabet[j];
                            }
                        }
                        if (skip == 0)
                        {
                            kata2[i] = alphabet[j];
                        }
                        else
                        {
                            kata2[i] = alphabet[j];
                        }
                        //kata2[i] = alphabet[j];
                    }
                }
            }

            Console.Write("Menjadi = ");
            for (i = 0; i < kata.Length; i++)
            {
                Console.Write(kata2[i]);
            }


            Console.WriteLine();
            Console.WriteLine("Press 1 to continue this number or any key to exit ");
            string choice = Console.ReadLine();
            if (choice == "1")
            {
                No4 execute = new();
            }
        }
    }
}
