﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    internal class No5
    {
        public No5()
        {
//            05 Maximum buying 3 items.Masukkan jumlah uang dan 3 jenis item dan harga-harganya.
//Input:
//50
//6 19 20
//29 19 8
//18 9 1
//Output:
//            48
//Penjelasan:
//            20 + 19 + 9 = 48
            Console.WriteLine();
            Console.WriteLine("===NO 5===");
            Console.WriteLine();

            Console.Write("Masukkan jumlah uang = ");
            int uang = int.Parse(Console.ReadLine());
            //int uang = 50;
            //Console.Write("Masukkan banyak item = ");
            //int banyakitem = int.Parse(Console.ReadLine());
            //int banyakitem = 3;
            //Console.Write("Masukkan banyak harga = ");
            //int banyakharga = int.Parse(Console.ReadLine());
            //int banyakharga = 3;
            //int[] item1 = { 6, 19, 20 };
            //int[] item2 = { 29, 19, 8 };
            //int[] item3 = { 18, 9, 1 };

            //int[,] item = { { 6, 19, 20 }, { 29, 19, 8 }, { 18, 9, 1 } };

            int i, j, k;

            int[] item1 = new int[3];
            int[] item2 = new int[3];
            int[] item3 = new int[3];

            //int i, j;
            Console.WriteLine("Masukkan 3 harga untuk item 1 = ");
            for (i = 0; i < 3; i++)
            {
                Console.Write($"Item 1 Harga[{i + 1}] = ");
                item1[i] = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Masukkan 3 harga untuk item 2 = ");
            for (i = 0; i < 3; i++)
            {
                Console.Write($"Item 1 Harga[{i + 1}] = ");
                item2[i] = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Masukkan 3 harga untuk item 3 = ");
            for (i = 0; i < 3; i++)
            {
                Console.Write($"Item 1 Harga[{i + 1}] = ");
                item3[i] = int.Parse(Console.ReadLine());
            }

            int max = 0;
            int selisih = uang;
            int jumlah;
            int n1 = 0, n2 = 0, n3 = 0;

            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 3; j++)
                {
                    for (k = 0; k < 3; k++)
                    {
                        jumlah = item1[i] + item2[j] + item3[k];
                        if (jumlah <= uang)
                        {
                            if (jumlah > max)
                            {
                                if (selisih > uang - jumlah)
                                {
                                    selisih = uang - jumlah;
                                    max = jumlah;
                                    n1 = i; n2 = j; n3 = k;
                                }
                            }
                        }
                    }
                }
            }

            Console.WriteLine($"Hasilnya adalah {item1[n1]} + {item2[n2]} + {item3[n3]} = {max}");

            Console.WriteLine();
            Console.WriteLine("Press 1 to continue this number or any key to exit ");
            string choice = Console.ReadLine();
            if (choice == "1")
            {
                No5 execute = new();
            }
        }
    }
}
