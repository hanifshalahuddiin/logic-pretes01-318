﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    internal class No7
    {
        public No7()
        {
//            07 Dalam satuan gugus tugas keamanan, Supri, Amir adalah mendapatkan cuti setelah menjelankan
//tugas beberapa hari.
//Supri mendapatkan libur setelah x hari kerja, sedangkan Amir mendapatkan libur setelah x
//hari kerja.
//Saat Supri &Amir libur secara bersamaan pada tanggal z, tanggal berapa mereka akan
//mendapatkan libur bersama?
//Input: int x, y; date / varchar z
//Output: tanggal libur bersama selanjutnya
//Input: x = 3, y = 2, z = 2023 - 05 - 20
//Output: 1 Juni 2023(2023 - 06 - 01)
            Console.WriteLine();
            Console.WriteLine("===NO 7===");
            Console.WriteLine();

            Console.Write("Masukkan hari ke berapa libur Supri = ");
            //int x = int.Parse(Console.ReadLine());
            int x = 3;
            Console.Write("Masukkan hari ke berapa libur Amir = ");
            //int y = int.Parse(Console.ReadLine());
            int y = 2;
            Console.Write("Masukkan hari libur mereka libur bersama (yyyy-mm-dd) = ");
            //string z = Console.ReadLine();
            string z = "2023-05-20";

            Console.WriteLine();
            Console.WriteLine("Press 1 to continue this number or any key to exit ");
            string choice = Console.ReadLine();
            if (choice == "1")
            {
                No7 execute = new();
            }
        }
    }
}
