﻿
//using Logic05;
// See https://aka.ms/new-console-template for more information

namespace Pretest04;

public class Program
{
    static void Main()
    {
    //Console.ReadKey();
    menu:
        Console.Clear();
        Console.WriteLine();
        Console.WriteLine("===PRETEST 04===");
        Console.WriteLine();
        Console.WriteLine("Pilih nomor soal di bawah ini:");
        Console.WriteLine("1.");
        Console.WriteLine("2.");
        Console.WriteLine("3.");
        Console.WriteLine("4.");
        Console.WriteLine("5.");
        Console.WriteLine("6.");
        Console.WriteLine("7.");
        Console.WriteLine("8.");
        Console.WriteLine();

        Console.Write("Masukkan nomor soal: ");
        string soal = Console.ReadLine();

        switch (soal)
        {
            case "1":
                No1 no1 = new No1();
                break;
            case "2":
                No2 no2 = new No2();
                break;
            case "3":
                No3 no3 = new No3();
                break;
            case "4":
                No4 no4 = new No4();
                break;
            case "5":
                No5 no5 = new No5();
                break;
            case "6":
                No6 no6 = new No6();
                break;
            case "7":
                No7 no7 = new No7();
                break;
            case "8":
                No8 no8 = new No8();
                break;
            default:
                Console.Write("Masukkan nomor soal yang tersedia");
                Console.ReadKey();
                Main();
                break;
        }

        goto menu;
    }


}

