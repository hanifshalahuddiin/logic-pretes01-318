﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    internal class No6
    {
        public No6()
        {
//            06 Hitunglah biaya parkir dengan range tarif 5000 untuk 1 jam pertama, 3000 untuk jam ke 2 - 10, 
//2000 untuk jam 11 - 23.Untuk 24 jam lansam 50000 dan sisanya dengan perhitungan regular.
//Input 1:
//2023 - 05 - 01T08: 10:55
//2023 - 05 - 01T17: 12:39
//Output 1:
//32000
//Penjelasan 1:
//1 x 5000 = 5000
//9 x 3000 = 27000
//-------------------- -
//10 jam = 32000
//Input 2:
//2023 - 05 - 01T07: 10:55
//2023 - 05 - 02T21: 12:39
//Output 2:
//92000
//Penjelasan 2:
//24 / 24 x 50000 = 50000
//1 x 5000 = 5000
//9 x 3000 = 27000
//5 X 2000 = 10000
//----------------------
//1 hari 15 jam = 92000
            Console.WriteLine();
            Console.WriteLine("===NO 6===");
            Console.WriteLine();

            //Console.Write("Masukkan tanggal dan waktu masuk : ");
            //string[] Masuk = Console.ReadLine().Split('T');
            //string[] tglMasuk = Masuk[0].Split('-');
            //string[] wktMasuk = Masuk[1].Split(':');
            //DateTime dateTimeMasuk = new DateTime(int.Parse(tglMasuk[0]), int.Parse(tglMasuk[1]), int.Parse(tglMasuk[2]), int.Parse(wktMasuk[0]), int.Parse(wktMasuk[1]), int.Parse(wktMasuk[2]));
            //Console.Write("Masukkan tanggal dan waktu keluar : ");
            //string[] Keluar = Console.ReadLine().Split('T');
            //string[] tglKeluar = Keluar[0].Split('-');
            //string[] wktKeluar = Keluar[1].Split(':');
            //DateTime dateTimeKeluar = new DateTime(int.Parse(tglKeluar[0]), int.Parse(tglKeluar[1]), int.Parse(tglKeluar[2]), int.Parse(wktKeluar[0]), int.Parse(wktKeluar[1]), int.Parse(wktKeluar[2]));


            Console.WriteLine();
            Console.WriteLine("Press 1 to continue this number or any key to exit ");
            string choice = Console.ReadLine();
            if (choice == "1")
            {
                No6 execute = new();
            }
        }
    }
}
