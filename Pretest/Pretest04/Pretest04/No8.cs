﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pretest04
{
    internal class No8
    {
        public No8()
        {
//            08 Angka dari fibbonacci dari urutan.
//Input 1:
//2
//Output 1:
//1
//Input 2:
//5
//Output 2:
//5
//Fibonacci:
//            1
//1
//2
//3
//5
//8
            Console.WriteLine();
            Console.WriteLine("===NO 8===");
            Console.WriteLine();

            Console.Write("Masukkan index = ");
            int index = int.Parse(Console.ReadLine());
            //int index = 10;

            int[] fibo = new int[index];

            fibo[0] = 1;
            fibo[1] = 1;

            int i, j;

            Console.Write($"Deret Fibonacci = {fibo[0]} {fibo[1]}");

            for (i = 2; i < index; i++)
            {
                fibo[i] = fibo[i - 2] + fibo[i - 1];
                Console.Write($" {fibo[i]}");
            }
            Console.WriteLine();
            Console.WriteLine($"Fibonacci ke-{index} adalah {fibo[index - 1]}");

            Console.WriteLine();
            Console.WriteLine("Press 1 to continue this number or any key to exit ");
            string choice = Console.ReadLine();
            if (choice == "1")
            {
                No8 execute = new();
            }
        }
    }
}
